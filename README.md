##Outlier
Em estatística, outlier, valor aberrante ou valor atípico, é uma observação que apresenta um grande afastamento das demais da série (que está "fora" dela), ou que é inconsistente.

##Aplicações
Detecção de fraudes

Detecção de intrusões

Perturbações em ecossistemas

##Referência
[1]Outlier, wikipedia, https://pt.wikipedia.org/wiki/Outlier