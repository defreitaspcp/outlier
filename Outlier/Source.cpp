/*
Author : de Freitas, P.C.P
Description - Outlier
References
[1] Outlier, wikipedia, https://en.wikipedia.org/wiki/Outlier
[2]Quick Sort, Drozdek, http://www.mathcs.duq.edu/drozdek/DSinCpp/sorts.h
*/
#include<iostream>
using namespace std;
struct Sort
{
public:
	void quicksort(double data[], int n)
	{
		int i, max;
		if (n < 2)
		{
			return;
		}
		for (i = 1, max = 0; i < n; i++)// find the largest
		{
			if (data[max] < data[i])// element and put it
			{
				max = i;                // at the end of data[];
			}
		}
		swap(data, n - 1, max); // largest el is now in its
		quicksort(data, 0, n - 2);     // final position;
	}
private:
	void swap(double data[], int i, int j)
	{
		double temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}
	void quicksort(double data[], int first, int last)
	{
		int lower = first + 1, upper = last;
		swap(data, first, (first + last) / 2);
		double bound = data[first];
		while (lower <= upper)
		{
			while (data[lower] < bound)
			{
				lower++;
			}
			while (bound < data[upper])
			{
				upper--;
			}
			if (lower < upper)
			{
				swap(data, lower++, upper--);
			}
			else
			{
				lower++;
			}
		}
		swap(data, upper, first);
		if (first < upper - 1)
		{
			quicksort(data, first, upper - 1);
		}
		if (upper + 1 < last)
		{
			quicksort(data, upper + 1, last);
		}
	}
};
double median(double data[], int length)
{
	Sort sort;
	sort.quicksort(data, length);
	if (length % 2 == 0)
	{
		return (data[length / 2 - 1] + data[length / 2]) / 2;
	}
	else
	{
		return data[length / 2];
	}
}
double medianLow(double data[], int length)
{
	Sort sort;
	sort.quicksort(data, length);
	if (length % 2 == 0)
	{
		return (data[length / 4 - 1] + data[length / 4]) / 2;
	}
	else
	{
		return data[length / 4];
	}
}
double medianUp(double data[], int length)
{
	Sort sort;
	sort.quicksort(data, length);
	if (length % 2 == 0)
	{
		return (data[length * 3 / 4 - 1] + data[length * 3 / 4]) / 2;
	}
	else
	{
		return data[length * 3 / 4];
	}
}
double mean(double data[], int length)
{
	double sum = 0.0;
	for (size_t i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double min(double data[], int length)
{
	Sort s;
	s.quicksort(data, length);
	return data[0];
}
double max(double data[], int length)
{
	Sort s;
	s.quicksort(data, length);
	return data[length-1];
}
double IQR(double data[], int length)
{
	return medianUp(data,length) - medianLow(data,length);
}
double outlier(double data[], int length)
{
	return IQR(data,length) * 1.5;
}
void displayCalculus(double data[], int lenght)
{
	cout << "Mean : " << mean(data, lenght) << endl;
	cout << "Min : " << min(data, lenght) << endl;
	cout << "Median low : " << medianLow(data, lenght) << endl;
	cout << "Median : " << median(data, lenght) << endl;
	cout << "Median up : " << medianUp(data, lenght) << endl;
	cout << "Max : " << max(data, lenght) << endl;
	cout << "IQR : " << IQR(data, lenght) << endl;
	cout << "Outlier : " << outlier(data, lenght) << endl;
	cout << "outlier is a value less than or equal to "
		<< medianLow(data, lenght) - outlier(data, lenght)
		<< " or greater than or equal to "
		<< medianUp(data, lenght) + outlier(data, lenght)
		<< endl;
	cout << endl;
}
int main()
{
	double CDAM20181[24] = { 1.49,1.09,0.96,1.22,1.24,1.32,1.21,0.99,0.96,1.06,1.02,1.05,
		1.02,0.96,1.18,0.99,0.98,1.86,0.99,1.15,1.21,1.08,1.06,0.96 };
	displayCalculus(CDAM20181, 24);

	double NEAM20181[24] = { 10,7.5,7.5,0,8,0,10,6,8.5,0,8.25,6.5,5,8.5,0,5.5,0,9,8.5,9,0,0,5.5,0 };
	displayCalculus(NEAM20181, 24);

	double CDAM20182[8] = { 1.02,0.99,0.84,1.06,1.02,1.02,0.99,0.99 };
	displayCalculus(CDAM20182, 8);

	double NEAM20182[8] = { 7,8.25,8,8.75,6.5,2.25,7,4.5 };
	displayCalculus(NEAM20182, 8);

	double CDES20181[11] = { 1.18,1.15,1.09,1.09,1.09,1.08,1.05,1.02,1.02,0.99,0.99 };
	displayCalculus(CDES20181, 11);

	double NEES20181[11] = { 0,0,7.5,0,8,0,4.5,7.5,3.5,6.5,7 };
	displayCalculus(NEES20181, 11);

	double CDAAM20181[11] = { 1.86,1.49,1.21,1.24,0.99,1.02,0.96,0.96,1.09,0.96,1.15 };
	displayCalculus(CDAAM20181, 11);

	double NEAAM20181[11] = { 9.0,10,10,8.0,8.5,8.25,8.5,8.5,7.5,7.5,9.0 };
	displayCalculus(NEAAM20181, 11);
	
	double NFAAM20181[11] = { 9.50,9.01,8.25,7.33,6.91,6.87,6.83,6.83,6.68,6.33,7.59 };
	displayCalculus(NFAAM20181, 11);

	double CDAAM20182[5] = { 1.06,1.02,0.99,0.84,0.99 };
	displayCalculus(CDAAM20182, 5);

	double NEAAM20182[5] = { 8.75,7.0,7.0,8.0,8.25 };
	displayCalculus(NEAAM20182, 5);

	double NFAAM20182[5] = { 9.38,8.35,8.21,7.97,8.83 };
	displayCalculus(NFAAM20182, 5);

	double CDAES20181[4] = { 1.09,1.09,1.02,0.99 };
	displayCalculus(CDAES20181, 4);

	double NEAES20181[4] = { 8.0,7.5,7.5,7.0 };
	displayCalculus(NEAES20181, 4);

	double NFAES20181[4] = { 8.62,8.37,8.07,7.69 };
	displayCalculus(NFAES20181, 4);

	return 0;
}